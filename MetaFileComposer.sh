#!/bin/bash

#set -x

ls /data/hu_qian/Downloads/MachineLearningPDFs/ > /tmp/MachineLearningLabels/labelDirectories # shows which label-directories are in that directory and saves the directory-names into a temporary list

numberOfLines=$(wc -l /tmp/MachineLearningLabels/labelDirectories)
numberOfLines=$(echo $numberOfLines | awk '{ print $1 }')

for ((lineNumber=1;lineNumber<=numberOfLines;lineNumber+=1))
do								
	line="$(awk "NR==$lineNumber" /tmp/MachineLearningLabels/labelDirectories)"
	ls /data/hu_qian/Downloads/MachineLearningPDFs/"$line"/ > /tmp/MachineLearningLabels/"$line" # goes through each directory-name of the list and shows the pdf-files in each directory; saves the pdf-filenames into new separate temporary lists
	numberOfFiles=$(wc -l /tmp/MachineLearningLabels/"$line")
	numberOfFiles=$(echo $numberOfFiles | awk '{ print $1 }')

	for ((fileNumber=1;fileNumber<=numberOfFiles;fileNumber+=1))
	do								
		file="$(awk "NR==$fileNumber" /tmp/MachineLearningLabels/"$line")"
		metaFilename="$file".meta
		touch /data/hu_qian/Downloads/MachineLearningPDFs/"$line"/"$metaFilename" # goes through each listed filename and creates a metafile for that file
		echo "label $line" > /data/hu_qian/Downloads/MachineLearningPDFs/"$line"/"$metaFilename" # writes into each metafile the specific labels (identical with the label directory name)
	done




done
